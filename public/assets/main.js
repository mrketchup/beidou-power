// API a consultar
const API = "https://api.genshin.dev"
const LANG = "?lang=ES"
const options = {method: 'GET', headers: {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'}};

// Convertir los datos recibidos a JSON
async function fetchData(urlApi){
    const response = await fetch(urlApi);
    const data = await response.json();
    return data;
}

// Recibir un personaje aleatorio
function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}


// Consunsumo de datos
const viewData = async (urlApi) => {
    try {
        const characters = await fetchData(`${urlApi}/characters${LANG}`);
        const personaje = await fetchData(`${urlApi}/characters/${characters[getRandomInt(34)]}${LANG}`);
        console.log(`
            Nombre: ${personaje.name}
            Titulo: ${personaje.title}
            Vision: ${personaje.vision}
            Descripcion: ${personaje.description}
            --- Talentos ---
        `)
        personaje.skillTalents.forEach(skills => {
            console.group();
            console.log(`Nombre: ${skills.name}`);
            console.log(`Talento: ${skills.unlock}`);
            console.log(`Descripcion: ${skills.description}`);
            console.groupEnd();
        });
    } catch (error) {
        console.log(error)
    }
}

viewData(API);

console.log("Mommy Beidou")